#include "Joystick.h"

Joystick::Joystick() {
  this->jsPinX = A0;    // default input pin for X axis
  this->jsPinY = A1;    // default input pin for Y axis
  pinMode(jsPinX, INPUT);
  pinMode(jsPinY, INPUT);
}

Joystick::Joystick(int jsPinX, int jsPinY) {
  this->jsPinX = jsPinX;
  this->jsPinY = jsPinY;
  pinMode(jsPinX, INPUT);
  pinMode(jsPinY, INPUT);
}

// return value: -99 ~ +99
int Joystick::getX() {
  int val = analogRead(jsPinX);
  if (val >= 512) val = map(val, 512, 992, 0, 99);
  else val = map(val, 0, 512, -99, 0);
  return val;
}

// return value: -99 ~ +99
int Joystick::getY() {
  int val = analogRead(jsPinY);
  Serial.println(val);
  if (val >= 516) val = map(val, 516, 992, 0, 99);
  else val = map(val, 0, 516, -99, 0);
  Serial.println(val);
  return val;
}
