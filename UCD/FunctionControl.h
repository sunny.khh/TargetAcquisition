#ifndef _FUNCTION_CONTROL_H
#define _FUNCTION_CONTROL_H

#include <LiquidCrystal.h>

enum SwitchState {
  NONE            = 0,
  SELECT_SINGLE   = 0x1 <<0,  // single press-release on select button
  SELECT_MULTI    = 0x1 <<1,  // multiple press-release on select button
  SELECT_LONG     = 0x1 <<2,  // long press on select button
  RESET_SINGLE    = 0x1 <<3,  // single press-release on reset button
  RESET_MULTI     = 0x1 <<4,  // multiple press-release on reset button
  RESET_LONG      = 0x1 <<5,  // long press on reset button
};

class FunctionControl {
  public:
    FunctionControl(LiquidCrystal *lcd);    // constructor
    SwitchState updateState();              // update function controller, return button events
    void displayState();                    // display the current state of function controller
    void displayMotorPos(float, float y);   // display the current motor positions
    void beep(int frequency, int duration); // sound a beep on buzzer
    int getSelected();                      // return what is selected on the controller
    void reset();
  
  private:
    int clickCount;
    bool selectPressed;
    bool prevSelectPressed;
    long lastTick;
    
    int selected;
    int aState;
    int aLastState;
    
    SwitchState prevBtnState;
    LiquidCrystal *lcd;
};

#endif
