#ifndef _JOYSTICK_H
#define _JOYSTICK_H

#include "Arduino.h"

class Joystick {
  public:
    Joystick();
    Joystick(int jsPinX, int jsPinY);
    
    int getX();
    int getY();
    
  private:
    int jsPinX;
    int jsPinY;
};
#endif
