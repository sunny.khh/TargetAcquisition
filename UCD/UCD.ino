#include <LiquidCrystal.h>
#include "Joystick.h"
#include "FunctionControl.h"
#include "Math.h"

#define UART_BAUD_RATE 115200
// Execution cycles of components in mini-second
const int JoystickCycle    =  50;    // Read joystick position.
const int ReadAtcCmdCycle  =  50;    // Read ATC commands.
const int ReadStateCycle   =   2;    // Read function selection knob.
const int CommMonitorCycle = 500;    // Test if ATC is comunicating.
const int TaskCycles[] = {ReadAtcCmdCycle, JoystickCycle, ReadStateCycle, CommMonitorCycle};

int GcdCycle;
int LcmCycle;

// LCD display configuration
const int d4 = 5, d5 = 4, d6 = 3, d7 = 2;   // data pins
const int rs = 12, en = 11;                 // control pins
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// Declare and initialize components
Joystick joy;
FunctionControl fc(&lcd);

Math math;


void displayJoystickPos(int x, int y) {
  char str[8];
  if (abs(x)<=1) x=0;
  if (abs(y)<=1) y=0;

  sprintf(str, "%4d", x);
  lcd.setCursor(16, 2);
  lcd.print(str);
  sprintf(str, "%4d", y);
  lcd.setCursor(16, 3);
  lcd.print(str);
}


// https://stackoverflow.com/questions/9072320/split-string-into-string-array
String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }

  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}


void setup() {
  Serial.begin(UART_BAUD_RATE);
  Serial.setTimeout(5);

  lcd.begin(20, 4);    // Set display size.
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Target Acquisition");
  lcd.setCursor(0, 2);

  fc.displayState();
  fc.displayMotorPos(0.0, 0.0);

  GcdCycle = math.gcd_m(TaskCycles, sizeof(TaskCycles)/sizeof(int));
  LcmCycle = math.lcm_m(TaskCycles, sizeof(TaskCycles)/sizeof(int));

  fc.beep(1000, 100);
}

bool commMonIndicatorOn = false;
bool commReceived = false;
bool atcConnected = false;
long lastLoopTime = 0;

void loop() {
  float lastMotorX = 0.0;
  float lastMotorY = 0.0;

  while(true) {
    for (int i=0; i<LcmCycle; i++) {
      if (i % (JoystickCycle/GcdCycle) == 0) {
        int joyX = joy.getX();
        int joyY = joy.getY();
        
        Serial.print("J ");
        Serial.print(joyX);
        Serial.print(" ");
        Serial.println(joyY);

        displayJoystickPos(joyX, joyY);
      }

      if (i % (ReadAtcCmdCycle/GcdCycle) == 0) {
          String cmd = Serial.readString();
          switch (cmd[0]) {
            
            case 'M':
              {
              String strX = getValue(cmd, ' ', 1);
              String strY = getValue(cmd, ' ', 2);
              float x = atof((char*)strX.c_str());
              float y = atof((char*)strY.c_str());
              fc.displayMotorPos(x, y);
              }
              break;

            case 'B':
              commReceived = true;
              break;

            case 'R':
              fc.reset();
              break;
          }
      }

      if (i % (ReadStateCycle/GcdCycle) == 0) {
        SwitchState state = fc.updateState();
        switch (state) {
          case SwitchState::SELECT_SINGLE:
            switch (fc.getSelected()) {
              case 0 ... 9:
                Serial.print("G ");
                Serial.println(fc.getSelected());
                break;
              case 10:
                // send traverse command
                Serial.println("T");
                break;
              case 11:
                // send reset command
                Serial.println("R");
                break;
            }
            break;
          case SwitchState::SELECT_MULTI:
            Serial.print("C ");
            Serial.println(fc.getSelected()); 
            break;
          case SwitchState::SELECT_LONG:
            Serial.print("S ");
            Serial.println(fc.getSelected()); 
            break;
        }
      }

      if (i % (CommMonitorCycle/GcdCycle) == 0) {
        if (commReceived) {
          commMonIndicatorOn = !commMonIndicatorOn;
          if (!atcConnected)
            fc.beep(1000 ,300);
          atcConnected = true;
        } else {
          commMonIndicatorOn = false;
          if (atcConnected)
            fc.beep(100, 50);
          atcConnected = false;
        }
        lcd.setCursor(19, 0);
        lcd.print(commMonIndicatorOn ? '*' : ' ');
        commReceived = false;
      }


#define CALIBRATING false       // set to true during time calibration
      long throttle = GcdCycle * 1000;
      long delayMicroSec;
      if (CALIBRATING) {
      // For time calibration only.  This is used during development only
        long currLoopTime = micros();
        long timelapse = currLoopTime - lastLoopTime; // timelapse since last loop
        lastLoopTime = currLoopTime;
        delayMicroSec = GcdCycle * 1000;
        Serial.print("T ");
        Serial.println(timelapse);
      } else
        delayMicroSec = GcdCycle * 1000 - 420;
        
      if (delayMicroSec > 0)
        delayMicroseconds(delayMicroSec);
    }
  }
}
