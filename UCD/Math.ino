#include "Math.h"

Math::Math() {}

int Math::gcd(int a, int b) {
    while(b) b ^= a ^= b ^= a %= b;
    return a;
}

int Math::lcm(int a, int b) {
    return a*b / gcd(a,b);
}

int Math::gcd_m(const int *nums, const size_t n) {
  if (n==0) return 1;
  if (n==1) return nums[0];

  int result = nums[0];
  for (int i=1; i<n; i++)
    result = gcd(result, nums[i]);
  return result;
}

int Math::lcm_m(const int* nums, const size_t n) {
  if (n==0) return 0;
  if (n==1) return nums[0];
  
  int result = nums[0];
  for (int i=1; i<n; i++)
    result = lcm(result, nums[i]);
  return result;
}
