#include "FunctionControl.h"

#define RotaryPinA  6   // rotary sensor input pins
#define RotaryPinB  7
#define SelectPbPin 8   // input pin for selection button
#define ResetPbPin  9   // input pin for reset button
#define BuzzerPin   10  // output pin for buzzer


FunctionControl::FunctionControl(LiquidCrystal *lcd):
    clickCount(0),
    selectPressed(false),
    prevSelectPressed(false),
    lastTick(0),
    selected(0)
{
  this->lcd = lcd;
  
  pinMode(RotaryPinA,  INPUT_PULLUP);
  pinMode(RotaryPinB,  INPUT_PULLUP);
  pinMode(SelectPbPin, INPUT_PULLUP);
  pinMode(ResetPbPin,  INPUT_PULLUP);
  pinMode(BuzzerPin,   OUTPUT);

  aLastState = aState = digitalRead(RotaryPinA);
  prevBtnState = SwitchState::NONE;
}


void FunctionControl::reset() {
  aLastState = aState = digitalRead(RotaryPinA);
  prevBtnState = SwitchState::NONE;
  clickCount = 0;
  selected = 0;
  displayMotorPos(0.0, 0.0);
  displayState();
}


SwitchState FunctionControl::updateState() {

  // Process rotary knob.
  aState = digitalRead(RotaryPinA);
  if (aState != aLastState) {
    if (digitalRead(RotaryPinB) != aState) {
      selected ++;
      if (selected > 11) {
        selected = 11;
        beep(5, 1); 
      } else
        beep(2000, 10); 
    } else {
      selected --;
      if (selected < 0) {
        selected = 0;
        beep(5, 1); 
      } else
        beep(2000, 10); 
    }
    displayState();
  }
  aLastState = aState;

  // Process push buttons.
  SwitchState btnState = SwitchState::NONE;
  long currTick = micros();

  selectPressed = !digitalRead(SelectPbPin);
  if (!prevSelectPressed && selectPressed) {
    lastTick = currTick;    
  }
  if (prevSelectPressed && !selectPressed) {
    clickCount ++;
  }

  long timelapse = currTick - lastTick;
  if (timelapse > 500000) {
    if (selectPressed && timelapse > 800000) {
      btnState = (SwitchState)(btnState | SwitchState::SELECT_LONG);
      lastTick = currTick;
      prevSelectPressed = selectPressed;
      beep(2000,100);
      clickCount = -1;
    } else {
      if (clickCount == 1) {
        btnState = (SwitchState)(btnState | SwitchState::SELECT_SINGLE);
        beep(2000,10);
      } else if (clickCount > 1) {
        btnState = (SwitchState)(btnState | SwitchState::SELECT_MULTI);
        beep(2000,50);
      }
      clickCount = 0;
    }
  }

  prevSelectPressed = selectPressed;
  return btnState;
}


void FunctionControl::beep(int frequency, int duration) {
  tone(BuzzerPin, frequency);
  delay(duration);
  noTone(BuzzerPin);
  delay(1);
}


int FunctionControl::getSelected() {
  return selected;
}


void FunctionControl::displayMotorPos(float x, float y) {
  char str[8];

  dtostrf(x, 7, 2, str);
  lcd->setCursor(0,3);
  lcd->print(str);
  dtostrf(y, 7, 2, str);
  lcd->setCursor(8,3);
  lcd->print(str);
}

void FunctionControl::displayState() {
  lcd->setCursor(0, 1);
  lcd->print("                  ");
  String str="0123456789 TR RS";

  if (selected < 10) {
    lcd->setCursor(selected, 1);
    lcd->print(selected);
    str[selected] = ' ';
    lcd->setCursor(0, 2);
    lcd->print(str);
  } else if (selected == 10) {
    lcd->setCursor(8, 1);
    lcd->print("TRAVERSE");
    str.replace("TR", "tr");
    lcd->setCursor(0, 2);
    lcd->print(str);
  } else if (selected == 11) {
    lcd->setCursor(13, 1);
    lcd->print("RESET");
    str.replace("RS", "rs");
    lcd->setCursor(0, 2);
    lcd->print(str);
  }
}
