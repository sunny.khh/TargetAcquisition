#ifndef _MATH_H
#define _MATH_H

class Math {
  public:
    Math();

    int gcd(const int a, const int b);
    int lcm(const int a, const int b);
    int gcd_m(const int *nums, const size_t n);
    int lcm_m(const int *nums, const size_t n);
};
#endif
