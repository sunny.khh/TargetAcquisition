from time import sleep
import RPi.GPIO as GPIO
from Stepper import Stepper

AzimLimitSwitchPin = 25
ElevLimitSwitchPin = 26

# Gimbal class provides controls of a gimbal object.
# It consists two motors: azimuth and elevation.
class Gimbal:

	def __init__(self, azimMotor, elevMotor, laser):
		self.azimMotor = azimMotor
		self.elevMotor = elevMotor
		self.laser = laser

		GPIO.setup(AzimLimitSwitchPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
		GPIO.setup(ElevLimitSwitchPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)


	# Find and move the gimbal to the home position
	def homeMotors(self):
		hiSpeed, miSpeed, loSpeed = 300, 200, 50
		checkLimitDelay = 0.00001
		try:
			self.laser.off()

			# Move shield tab away from azim sensor.
			self.azimMotor.turnAngle(-30.0, hiSpeed)
			while GPIO.input(AzimLimitSwitchPin) == GPIO.HIGH:
				sleep(checkLimitDelay)
			self.azimMotor.stop()

			# Move shield tab until it reach azim sensor.
			self.azimMotor.turnAngle(360.0, hiSpeed)
			while GPIO.input(AzimLimitSwitchPin) == GPIO.LOW:
				sleep(checkLimitDelay)
			self.azimMotor.stop()

			# more precide measurement from both sides
			self.azimMotor.turnAngle(-5.0, loSpeed)
			self.azimMotor.wait()
			self.azimMotor.turnAngle(10.0, loSpeed)
			while GPIO.input(AzimLimitSwitchPin) == GPIO.LOW:
				sleep(checkLimitDelay)
			self.azimMotor.stop()
			a0 = self.azimMotor.getAngle()

			self.azimMotor.turnAngle(10.0, loSpeed)
			self.azimMotor.wait()
			self.azimMotor.turnAngle(-10.0, loSpeed)
			while GPIO.input(AzimLimitSwitchPin) == GPIO.LOW:
				sleep(checkLimitDelay)
			self.azimMotor.stop()
			a1 = self.azimMotor.getAngle()

			# Take average of both sides.
			self.azimMotor.setAngle((a0+a1)/2)
			self.azimMotor.wait()

			# Move azim to an angle where laser may reach elev sensor.
			self.azimMotor.turnAngle(-15.0, miSpeed)
			self.azimMotor.wait()

			self.laser.on()
			# Move laser away from elev sensor.
			self.elevMotor.turnAngle(30.0, hiSpeed)
			while GPIO.input(ElevLimitSwitchPin) == GPIO.LOW:
				sleep(checkLimitDelay)
			self.elevMotor.stop()

			self.elevMotor.turnAngle(-360.0, hiSpeed)
			while GPIO.input(ElevLimitSwitchPin) == GPIO.HIGH:
				sleep(checkLimitDelay)
			self.elevMotor.stop()

			# more precide measurement from both sides
			self.elevMotor.turnAngle(5.0, loSpeed)
			self.elevMotor.wait()
			self.elevMotor.turnAngle(-10.0, loSpeed)
			while GPIO.input(ElevLimitSwitchPin) == GPIO.HIGH:
				sleep(checkLimitDelay)
			self.elevMotor.stop()
			a0 = self.elevMotor.getAngle()

			self.elevMotor.turnAngle(-8.0, loSpeed)
			self.elevMotor.wait()
			self.elevMotor.turnAngle(+8.0, loSpeed)
			while GPIO.input(ElevLimitSwitchPin) == GPIO.HIGH:
				sleep(checkLimitDelay)
			self.elevMotor.stop()
			a1 = self.elevMotor.getAngle()

			# Take average of both sides.
			self.elevMotor.setAngle((a0+a1)/2)
			self.elevMotor.wait()
			self.elevMotor.reset()

			self.azimMotor.turnAngle(-32.0, hiSpeed)
			self.elevMotor.turnAngle(119.25, hiSpeed)
			self.azimMotor.wait();
			self.elevMotor.wait();

			self.azimMotor.reset()
			self.elevMotor.reset()

		except (KeyboardInterrupt, SystemExit): # when you press ctrl-c
			self.cleanup()
			raise


	# Stop motors and turn off laser.
	def cleanup(self):
		self.azimMotor.kill()
		self.elevMotor.kill()
		self.azimMotor.join()
		self.elevMotor.join()
		self.laser.off()


	# Move the gimbal at the given speed.
	def slew(self, speedX, speedY):
		if speedX == 0:
			self.azimMotor.stop()
		elif speedX > 0:
			self.azimMotor.slew(Stepper.DIRECTION.CW, abs(speedX))
		else:
			self.azimMotor.slew(Stepper.DIRECTION.CCW, abs(speedX))

		if speedY ==  0:
			self.elevMotor.stop()
		elif speedY > 0:
			self.elevMotor.slew(Stepper.DIRECTION.CCW, abs(speedY))
		else:
			self.elevMotor.slew(Stepper.DIRECTION.CW, abs(speedY))


	# Convert motor coordinates system to the earth coordinates system.
	def azimElev2LonLat(self, azim, elev):   # conver angles to lat/lon
		while azim >   180.0: azim -= 360.0
		while azim <= -180.0: azim += 360.0
		while elev >   180.0: elev -= 360.0
		while elev <= -180.0: elev += 360.0

		if elev > 90.0:
			elev = 180.0 - elev
			if azim > 0:
				azim -= 180
			else:
				azim += 180
		elif elev < -90.0:
			elev = -180.0 - elev
			if azim > 0:
				azim -= 180
			else:
				azim += 180

		return (azim, elev)

