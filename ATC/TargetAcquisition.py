from time import sleep
import RPi.GPIO as GPIO
import serial

from Stepper import Stepper
from Gimbal import Gimbal
from Laser import Laser

UART_BAUD_RATE = 115200
NUM_TARGETS = 10           # the max number of targets to track

LaserPin = 12

uart = serial.Serial(
	port     = '/dev/ttyS0',
	baudrate = UART_BAUD_RATE,
	parity   = serial.PARITY_NONE,
	stopbits = serial.STOPBITS_ONE,
	bytesize = serial.EIGHTBITS,
	timeout  = 1
)


# This class is the core of the Target Acquisition Project.
class TargetAcquisition:

	# constructor
	def __init__(self):
		GPIO.setmode(GPIO.BCM)

		self.targetAngles = []
		for i in range(NUM_TARGETS):
			self.targetAngles.append((float('inf'), float('inf')))
		
		self.slewMode = True

		azimMotor = Stepper(dirPin=23, stepPin=24, enablePin=18)
		elevMotor = Stepper(dirPin=20, stepPin=21, enablePin=16)
		laser = Laser(LaserPin)
		self.gimbal = Gimbal(azimMotor, elevMotor, laser)

		uart.write('R'.encode())	# set reset

		self.lastMotorX, self.lastMotorY = 0.0, 0.0
		cmd = 'M {} {}'.format(self.lastMotorX, self.lastMotorY);
		uart.write(cmd.encode())


	# Read a line of command from UART.
	def readControlCmd(self):
		try:
			str = uart.readline().decode()
		except UnicodeDecodeError as e:
			print('UART read error: {}'.format(e))
			str = ''
		return str


	# Clean up GPIO settings.
	def cleanup(self):
		self.gimbal.cleanup()
		GPIO.cleanup()


	# Convert joystick position to motor speed.
	def posToSpeed(self, pos):
		if   abs(pos) <  3: speed = 0
		elif abs(pos) < 20: speed = 2
		elif abs(pos) < 40: speed = 5
		elif abs(pos) < 50: speed = 10
		elif abs(pos) < 60: speed = 30
		elif abs(pos) < 70: speed = 60
		elif abs(pos) < 80: speed = 100
		else: speed = 250

		if (pos<0): speed = -speed
		return speed


	# Blink the laser dot.
	def blinkLaser(self, count=3):
		for i in range(count):
			sleep(0.2)
			self.gimbal.laser.off()
			sleep(0.2)
			self.gimbal.laser.on()


	# Move motors to (x,y).
	def gotoXY(self, x, y):
		if x == float('inf') or y == float('inf'):
			return
		slewMode = False
		speed = 200
		self.gimbal.azimMotor.setSpeed(speed)
		self.gimbal.azimMotor.setAngle(x)
		self.gimbal.elevMotor.setSpeed(speed)
		self.gimbal.elevMotor.setAngle(y)

		# tune speeds to sync the two motors
		stepsX = abs(self.gimbal.azimMotor.getStepsToGo())
		stepsY = abs(self.gimbal.elevMotor.getStepsToGo())
		if (stepsX < stepsY):
			reducedSpeed = round(float(speed)*stepsX/stepsY)
			self.gimbal.azimMotor.setSpeed(reducedSpeed)
		elif (stepsX > stepsY):
			reducedSpeed = round(float(speed)*stepsY/stepsX)
			self.gimbal.elevMotor.setSpeed(reducedSpeed)

		self.gimbal.azimMotor.wait()
		self.gimbal.elevMotor.wait()
		slewMode = True


	# Visit all preset targets.
	def traverse(self):
		for i in range(NUM_TARGETS):
			x, y = self.targetAngles[i]
			if x != float('inf') and y != float('inf'):
				self.gotoXY(x, y)
				self.blinkLaser()
				sleep(1)


	# Send reset command to UCD.
	def reset(self):
		self.gimbal.homeMotors()
		cmd = 'R'
		uart.write(cmd.encode())


	# The main program starts here.
	def start(self):
		self.gimbal.azimMotor.start()
		self.gimbal.elevMotor.start()

		self.reset()

		self.slewMode = True
		running = True
		iter = 0
		while running:
			try:
				iter += 1;
				if iter>=65536: iter=0

				sleep(0.0001)
				cmdStr = self.readControlCmd()
				if not cmdStr: continue

				x, y = 0, 0
				tokens = cmdStr.split()
				if tokens[0].upper() == 'J' and len(tokens) == 3:  # read joystick position
					# process joysyick input
					try:
						x, y = int(tokens[1]), int(tokens[2])
						# print('Joystick: {}, {}'.format(x,y))  #debug
					except ValueError as e:
						print('Bad command: {}'.format(cmdStr))
						print(e)

					if self.slewMode:
						self.gimbal.slew(self.posToSpeed(x), self.posToSpeed(y))

				elif tokens[0].upper() == 'S' and len(tokens) == 2:  # store target
					# save a selected target
					try:
						selected = int(tokens[1])
						if selected < NUM_TARGETS:
							self.targetAngles[selected] = (self.lastMotorX, self.lastMotorY)
						else:
							raise IndexError('Target store index out of range: {}'.\
									format(selected))
					except ValueError as e:
						print('Bad command: {}'.format(cmdStr))
						print(e)

				elif tokens[0].upper() == 'G' and len(tokens) == 2:  # visit a target
					# visit a selected target
					try:
						selected = int(tokens[1])
						if selected < NUM_TARGETS:
							x, y = self.targetAngles[selected]
							if x != float('inf') and y != float('inf'):
								self.gotoXY(x, y)
								self.blinkLaser()
						else:
							raise IndexError('Target store index out of range: {}'.\
									format(selected))
					except ValueError as e:
						print('Bad command: {}'.format(cmdStr))
						print(e)

				elif tokens[0].upper() == 'T' and len(tokens) == 1:  # Reset
					# traverse all preset targets
					self.traverse()

				elif tokens[0].upper() == 'R' and len(tokens) == 1:  # Reset
					# reset ATC
					self.reset()

				if iter%8 == 0:
					x, y = self.gimbal.azimMotor.getAngle(), self.gimbal.elevMotor.getAngle()
					x, y = self.gimbal.azimElev2LonLat(x, y)
					if self.lastMotorX != x or self.lastMotorY != y :
						cmd = 'M {} {}'.format(x, y)
						# print(cmd)
						uart.write(cmd.encode())
						self.lastMotorX, self.lastMotorY = x, y

				if iter%4 == 0:
					cmd = 'B'
					uart.write(cmd.encode())

			except (KeyboardInterrupt, SystemExit):  # when you press ctrl-c
				self.cleanup()
				raise


ta = TargetAcquisition()
ta.start()
cleanup()
exit();
