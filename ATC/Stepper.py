from time import sleep
import RPi.GPIO as GPIO
import threading
from threading import Thread
from enum import IntEnum
from datetime import datetime


# Stepper class provides control to a stepper motor.
class Stepper(Thread):

	# motor rotation direction
	class DIRECTION(IntEnum):
		CCW = GPIO.LOW    # counter clockwise
		CW  = GPIO.HIGH   # clockwise

	# This class variable is used to cleanup GPIOs when all motors are shutdown.
	MotorCount = 0        # total number of this type of motors

	# These are constants for the physical desing of the motor and gears.
	MicroSteps  = 4;
	DegPerStep  = 1.8 / MicroSteps
	GearRatio   = 6.0
	DegPerStep /= GearRatio 

	# shortest cycle (fast speed) possible
	# Step faster than this causes motor to jitter.
	MinDelayPerStep = 0.0005 / MicroSteps

	MaxSpeed = (int)(DegPerStep / MinDelayPerStep) 


	# constructor
	def __init__(self, dirPin=20, stepPin=21, enablePin=16):

		Thread.__init__(self)
		self.running = False
		self.lastRunTime = datetime.now()

		self._key_lock = threading.Lock()

		self.DIR    = dirPin    # Direction GPIO Pin
		self.STEP   = stepPin   # Step GPIO Pin
		self.ENABLE = enablePin # Enable GPIO Pin

		GPIO.setmode(GPIO.BCM)
		GPIO.setup(self.DIR,    GPIO.OUT)
		GPIO.setup(self.STEP,   GPIO.OUT)
		GPIO.setup(self.ENABLE, GPIO.OUT)

		self.reset()
		Stepper.MotorCount += 1


	# destructor
	def __del__(self):
		Stepper.MotorCount -= 1
		self.running = False
		

	# Reset the motor.
	def reset(self):
		#self.disable()
		self.lastRunTime = datetime.now()
		self.angle = 0.0
		self.stepsToGo = 0
		self.direction = Stepper.DIRECTION.CW
		self.delay = self.MinDelayPerStep


	# Return the number of steps until the target position.
	def getStepsToGo(self):
		return self.stepsToGo


	# override run() of the Thread class
	def run(self):
		while self.running:
			if self.stepsToGo == 0:
				timeSpan = datetime.now() - self.lastRunTime
				if timeSpan.total_seconds() > 30:
					self.disable()
				sleep(0.05)
				#sleep(0.01)
				#self.disable()
			else:
				self.enable()
				while self.stepsToGo > 0:
					GPIO.output(self.DIR, Stepper.DIRECTION.CW)
					GPIO.output(self.STEP, GPIO.HIGH)
					sleep(self.delay)
					GPIO.output(self.STEP, GPIO.LOW)
					sleep(self.delay)

					self._key_lock.acquire()
					self.stepsToGo -= 1
					self.angle += self.DegPerStep
					if self.angle > 180.0:
						self.angle -= 360.0
					self._key_lock.release()

				sleep(0.01)

				while self.stepsToGo < 0:
					GPIO.output(self.DIR, Stepper.DIRECTION.CCW)
					GPIO.output(self.STEP, GPIO.HIGH)
					sleep(self.delay)
					GPIO.output(self.STEP, GPIO.LOW)
					sleep(self.delay)

					self._key_lock.acquire()
					self.stepsToGo += 1
					self.angle -= self.DegPerStep
					if self.angle <= -180.0:
						self.angle += 360.0
					self._key_lock.release()

				sleep(0.01)

				self.lastRunTime = datetime.now()


	# Initiate the motor thread.
	def start(self):
		self.running = True
		Thread.start(self)


	# Terminate the motor thread.
	def kill(self):
		self.running = False
		GPIO.setup(self.DIR,    GPIO.IN)
		GPIO.setup(self.STEP,   GPIO.IN)
		GPIO.setup(self.ENABLE, GPIO.IN)


	# Set the running speed of the mortor.
	def setSpeed(self, speed):    # speed in degs/sec
		if speed < 2:
			speed = 2 
		elif speed > self.MaxSpeed:
			speed = self.MaxSpeed
		self.delay = self.DegPerStep / speed


	# Stop the motor immediately at the current position.
	def stop(self):
		self._key_lock.acquire()
		self.stepsToGo = 0
		self._key_lock.release()


	# Set the target angle and start moving the mortor.
	def setAngle(self, angle):
		self._key_lock.acquire()
		delta = angle - self.angle
		while delta > 180:
			delta -= 360
		while delta <= -180:
			delta += 360
		self.stepsToGo = int(round(delta / self.DegPerStep))
		self._key_lock.release()


	# Get the current angle.
	def getAngle(self):
		return self.angle


	# Move the motor by given angle at a given speed.
	def turnAngle(self, delta, speed=100):
		self._key_lock.acquire()
		self.setSpeed(speed)
		self.stepsToGo = int(round(delta / self.DegPerStep))
		self._key_lock.release()


	# Move the mortor at the given direction and speed.
	def slew(self, dir, speed=30):
		if speed==0:
			self.stepsToGo = 0;
			return

		self.setSpeed(speed)
		if dir == Stepper.DIRECTION.CW:
			self.setAngle(self.angle + 20)
		elif dir == Stepper.DIRECTION.CCW:
			self.setAngle(self.angle - 20)


	# Enable the mortor and lock in position when it is not running.
	def enable(self):
		GPIO.output(self.ENABLE, GPIO.LOW)


	# Disable the mortor.
	# Motor is unlocked and can be freely moved.
	def disable(self):
		GPIO.output(self.ENABLE, GPIO.HIGH)


	# Pause the current process until the mortor reach its target position.
	def wait(self):
		while self.stepsToGo != 0:
			sleep(0.01)


# # unit test
#motor1 = Stepper(dirPin=23, stepPin=24, enablePin=18)
#motor1.start()
#motor2 = Stepper(dirPin=20, stepPin=21, enablePin=16)
#motor2.start()
# 
#motor1.setAngle(180)
#motor2.setAngle(180)
#motor1.wait()
#motor2.wait()
#motor1.kill()
#motor2.kill()

