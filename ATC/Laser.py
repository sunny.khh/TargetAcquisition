import RPi.GPIO as GPIO

# Laser calss provides controls to the laser emitter.
class Laser:

	def __init__(self, laserPin):
		self.laserPin = laserPin
		GPIO.setup(self.laserPin, GPIO.OUT)

	def on(self):
		GPIO.output(self.laserPin, GPIO.HIGH)

	def off(self):
		GPIO.output(self.laserPin, GPIO.LOW)
